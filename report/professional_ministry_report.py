# -*- coding: utf-8 -*-

from trytond.pool import Pool
from trytond.report import Report

from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta
from trytond.transaction import Transaction


__all__ = ['MinistryByProfessionalReport']

class MinistryByProfessionalReport(Report):
    'Ministry by Professional Report'
    __name__ = 'gnuhealth.dentistry.ministry_report'
    
    
    @classmethod
    def get_context(cls, records, data):
        pool = Pool()
        Appointments = pool.get('gnuhealth.appointment')
        Treatments = pool.get('gnuhealth.dentistry.treatment')
        User = Pool().get('res.user')
        user = User(Transaction().user)
       
        
        context = super(MinistryByProfessionalReport, cls).get_context(records, data) #es un diccionario
        
        if data:
            start = data['start']
            end = data['end']
            healthprof = data['healthprof']
            start_d = start.date()
            end_d = end.date()
        
            appointment_check = Appointments.search([
                                ('appointment_date','>=',start),
                                ('appointment_date','<=',end),
                                ('state','=','checked_in'),
                                ('healthprof','=',healthprof),
                                ('healthprof.institution.name.name','=',user.company.rec_name)])
        
            appointment_done = Appointments.search([
                                ('appointment_date','>=',start),
                                ('appointment_date','<=',end),
                                ('state','=','done'),
                                ('healthprof','=',healthprof),
                                ('healthprof.institution.name.name','=',user.company.rec_name)])
            
           
            treatments = Treatments.search([
                                ('treatment_date','>=',start_d),
                                ('treatment_date','<=',end_d),
                                ('healthprof','=',healthprof),
                                ('healthprof.institution.name.name','=',user.company.rec_name)])
            
                        
            context['start_date'] = start
            context['end_date'] = end
            context['current_date'] = date.today()
            context['dentist'] = ''

            #Totales de atenciones ya sea por turno o por tratamiento. Si coincide un tratamiento con un turno el mismo día, se cuenta sólo una sola vez
            
            context['total'] =0
            
            #Hacemos un vector con las fechas y el DNI del paciente según los turnos registrados
            context['appointment_ref_date'] = []
            
            for x in appointment_check:
                 aux=[]
                 aux.append(x.patient.name.ref)
                 aux.append(x.appointment_date)
                 context['appointment_ref_date'].append(aux)
            
            for x in appointment_done:
                 aux=[]
                 aux.append(x.patient.name.ref)
                 aux.append(x.appointment_date)
                 context['appointment_ref_date'].append(aux)
            
            for x in treatments:
                for y in context['appointment_ref_date']:
                    if x.patient.name.ref==y[0] and x.treatment_date ==y[1]:
                        pass
                    else:
                        aux=[]
                        aux.append(x.patient.name.ref)
                        aux.append(x.treatment_date)
                        context['appointment_ref_date'].append(aux)
                        
            context['total'] = len (context['appointment_ref_date'])
            
            
            #Para trabajar con los Tratamientos Odontológicos
        
            context['first_time'] = 0
            context['urgency'] = 0
            context['attendance'] = 0
            context['basic_discharge'] = 0
            context['middle_discharge'] = 0
            context['integral_discharge'] = 0            
            
            for x in treatments:
                context['dentist'] = x.healthprof.rec_name
                if x.first_time==True:
                    context['first_time'] +=1
                
            for x in treatments:
                if x.urgency==True:
                    context['urgency'] +=1
            
            for x in treatments:
                if x.attendance==True:
                    context['attendance'] +=1
            
            context['urgency_attendance'] =0
            context['urgency_attendance']= context['urgency']  + context['attendance']
           
            for x in treatments:
                if x.discharge == 'basic':
                    context['basic_discharge'] +=1
                elif x.discharge == 'middle':
                    context['middle_discharge'] +=1
                elif x.discharge == 'integral':
                    context['integral_discharge'] +=1
            
            #Para trabajar con los procedimientos

            context['extraction'] = 0
            context['prevention'] = 0
            context['inactivation'] = 0
            context['restoration'] = 0
            context['treatments_pulpar'] = 0
            context['protesis'] = 0
            context['treatments_maloclusion'] = 0
            for i in treatments:
                aux=i.procedures
                for j in aux:
                    if j.procedure.code == 'EXTRACC':
                        context['extraction'] += 1
                    elif j.procedure.code == 'HIGIENEORAL' or j.procedure.code == 'ASESORDIETARIO' or j.procedure.code == 'ENFPERIODONTALES' or j.procedure.code == 'LIMPIEZAFLUOR' or j.procedure.code == 'SELLADO' or j.procedure.code == 'PREVENCION':
                        context['prevention'] += 1     
                    elif j.procedure.code == 'INACTIVAC':
                        context['inactivation'] += 1
                    elif j.procedure.code == 'OBTURACION':
                        context['restoration'] += 1
                    elif j.procedure.code == 'FORMOCRESOL' or j.procedure.code == 'CONDUCTO':
                        context['treatments_pulpar'] +=1
                    elif j.procedure.code == 'PROTESIS':
                        context['protesis'] += 1 
                    elif j.procedure.code == 'ORTODONCIA' or j.procedure.code == 'ORTOPEDIA':
                        context['treatments_maloclusion'] += 1             

              

        return context






