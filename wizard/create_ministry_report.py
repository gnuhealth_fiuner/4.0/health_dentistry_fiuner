from trytond.wizard import Wizard, StateView, Button, StateTransition, StateAction
from trytond.model import ModelView, fields
from trytond.pool import Pool
from trytond.exceptions import UserError

__all__ = ['CreateMinistryReportStart', 'CreateMinistryReportWizard']


class CreateMinistryReportStart(ModelView):
    'Ministry Report by Professional Start'
    __name__ = 'gnuhealth.dentistry.ministry_report.start'

    report_ = fields.Selection([
        ('create_ministry_report', 'Ministry Report by Professional'),
         ],'Ministry Report by Professional',required=True,sort=False)
    start_date = fields.DateTime('Start Date', required=True)
    end_date = fields.DateTime('End Date', required=True)
    professional = fields.Many2One('gnuhealth.healthprofessional',
        'Health Prof', help="Health professional", required=True)

    @staticmethod
    def default_professional():
        pool = Pool()
        HealthProf = pool.get('gnuhealth.healthprofessional')
        hp = HealthProf.get_health_professional()
        return hp

    @staticmethod
    def default_report_():
        return 'create_ministry_report'


class CreateMinistryReportWizard(Wizard):
    'Ministry Report by Professional Wizard'
    __name__ = 'gnuhealth.dentistry.ministry_report.wizard'

    @classmethod
    def __setup__(cls):
        super(CreateMinistryReportWizard,cls).__setup__()
        #cls._error_messages.update({
            #'end_date_before_start_date': 'The end date cannot be major thant the start date',
            #})

    start = StateView('gnuhealth.dentistry.ministry_report.start',
                      'health_dentistry_fiuner.create_ministry_report_start_view',[
                        Button('Cancel','end','tryton-cancel'),
                        Button('Print Report','prevalidate','tryton-ok',default=True),
                       ])

    prevalidate = StateTransition()

    create_ministry_report =\
        StateAction('health_dentistry_fiuner.act_gnuhealth_ministry_report')

    def transition_prevalidate(self):
        if self.start.end_date < self.start.start_date:
            raise UserError('health_dentistry_fiuner.end_date_before_start_date')
        return self.start.report_

    def fill_data(self):
        start = self.start.start_date
        end = self.start.end_date
        healthprof = self.start.professional.id
        return {
            'start':start, 
            'end':end,
            'healthprof':healthprof
            }
        
    def do_create_ministry_report(self, action):
        data = self.fill_data()
        return action, data
