# -*- coding: utf-8 -*-
##############################################################################
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from trytond.model import ModelView
from trytond.wizard import Wizard, StateTransition, StateAction, StateView, Button
from trytond.transaction import Transaction
from trytond.pool import Pool
from trytond.pyson import PYSONEncoder
from trytond.exceptions import UserError


__all__ = ['OpenAppointmentDentistryTreatment']

class OpenAppointmentDentistryTreatment(Wizard):
    'Create Appointment Evaluation'
    __name__ = 'wizard.gnuhealth.appointment.dentistry_treatment'

    start_state = 'appointment_dentistry_treatment'
    appointment_dentistry_treatment = StateAction('health_dentistry_fiuner.act_app_dentistry_treatment')

    def do_appointment_dentistry_treatment(self, action):
        pool = Pool()
        DentistryTreatment = pool.get('gnuhealth.dentistry.treatment')

        appointment = Transaction().context.get('active_id')

        try:
            app_id = \
                Pool().get('gnuhealth.appointment').browse([appointment])[0]
        except:
            raise UserError('health_dentistry_fiuner.no_record_selected')

        try:
            patient_id = app_id.patient.id
        except:
            raise UserError('health_dentistry_fiuner.no_patient')

        dentistry_treatments = DentistryTreatment.search(['patient','=',patient_id])

        action['pyson_domain'] = PYSONEncoder().encode([
            ('patient','=',app_id.patient.id),
            ])
        action['pyson_context'] = PYSONEncoder().encode({
            'patient': app_id.patient.id,
            })

        data = {'res_id': [x.id for x in dentistry_treatments]}
        return action, data

    @classmethod
    def __setup__(cls):
        super(OpenAppointmentDentistryTreatment, cls).__setup__()
        #cls._error_messages.update({
            #'no_record_selected':
                #'You need to select one Appointment record',
            #'no_patient':
                #'This appointment has no patient',
        #})

