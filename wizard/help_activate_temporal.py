from trytond.wizard import Wizard, StateView, Button
from trytond.model import ModelView, fields


__all__ = ['HelpActivateTemporalStart', 'HelpActivateTemporalWizard']


class HelpActivateTemporalStart(ModelView):    
    'Help for activate Temporal Odontogram Start'
    __name__ = 'gnuhealth.dentistry.help_activate_temporal.start'

    text = fields.Text('How activate the temporal schema for the odontogram',
                       readonly=True)

    @staticmethod
    def default_text():
        return ("1- Buscar el Paciente en la lista de pacientes\n"
            "2- Hacer doble click en el paciente para ingresar a su ficha personal\n"
            "3- Ir a la pestaña 'Odontología''\n"
            "4- Tildar en el campo 'Esquema temporal'\n"
            "5- El 'esquema temporal' aparece en el sistema\n"
            "6- El esquema temporal ya se encuentra disponible para editar en el Odontograma")


class HelpActivateTemporalWizard(Wizard):
    'Help for activate Temporal Odontogram Wizard'
    __name__ = 'gnuhealth.dentistry.help_activate_temporal.wizard'
    
    start = StateView('gnuhealth.dentistry.help_activate_temporal.start',
                      'health_dentistry_fiuner.help_activate_temporal_start_view',[
                        Button('Close','end','tryton-cancel', default=True),
                        ])
    
